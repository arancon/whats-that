package imaginecode.thecomebacks.whatsthat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhatsThatApplication {

	public static void main(String[] args) {
		SpringApplication.run(WhatsThatApplication.class, args);
	}
}
