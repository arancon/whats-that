package imaginecode.thecomebacks.whatsthat.repository;

import org.springframework.data.repository.CrudRepository;

import imaginecode.thecomebacks.whatsthat.entity.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	User findFirstByUsername(String username);
}