package imaginecode.thecomebacks.whatsthat.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import imaginecode.thecomebacks.whatsthat.entity.Image;

public interface ImageRepository extends CrudRepository<Image, Integer> {
	@Query("SELECT i FROM Image i WHERE i.id > ?1")
	List<Image> findFirstByOrderByIdAsc(int id);
}