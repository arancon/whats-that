package imaginecode.thecomebacks.whatsthat.control;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import imaginecode.thecomebacks.whatsthat.beans.UserResponse;
import imaginecode.thecomebacks.whatsthat.repository.ImageRepository;
import imaginecode.thecomebacks.whatsthat.repository.UserRepository;
import imaginecode.thecomebacks.whatsthat.service.BlockchainService;

@RestController
public class AnswerEndpoint {

	@Autowired UserRepository userRepository;
	@Autowired BlockchainService blockchainService;
	@Autowired ImageRepository imageRepository;

	@PostMapping("/answer")
	public boolean answer(@RequestBody UserResponse ur) throws Exception {
		return blockchainService.answer(ur);
	}
	
	@PostMapping("/points")
	public BigInteger getPoints(@RequestBody UserResponse ur) throws Exception {
		return blockchainService.getPoints(ur);
	}
	
}
