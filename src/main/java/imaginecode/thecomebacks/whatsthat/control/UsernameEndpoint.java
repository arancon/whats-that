package imaginecode.thecomebacks.whatsthat.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import imaginecode.thecomebacks.whatsthat.beans.UserResponse;
import imaginecode.thecomebacks.whatsthat.service.UserService;

@RestController
public class UsernameEndpoint {
	
	@Autowired UserService userService;
	
	@PutMapping("/user")
	public boolean answer(@RequestBody UserResponse ur) throws Exception {
		return userService.isUserLogged(ur);
	}
	
	@PostMapping("/user")
	public boolean getPoints(@RequestBody UserResponse ur) throws Exception {
		return userService.createUser(ur);
	}
	
}
