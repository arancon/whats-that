package imaginecode.thecomebacks.whatsthat.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import imaginecode.thecomebacks.whatsthat.beans.UserResponse;
import imaginecode.thecomebacks.whatsthat.entity.Image;
import imaginecode.thecomebacks.whatsthat.service.ImageService;

@RestController
public class ImageEndpoint {
	@Autowired ImageService imageService;

	@PostMapping("/image")
	public Image getImage(@RequestBody UserResponse ur) {
		return imageService.getImage(ur);
	}
	
	@PostMapping("/newimage")
	public boolean postImage(@RequestBody UserResponse ur) throws Exception {
		return imageService.addImage(ur);
	}
}
