package imaginecode.thecomebacks.whatsthat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import imaginecode.thecomebacks.whatsthat.beans.UserResponse;
import imaginecode.thecomebacks.whatsthat.entity.User;
import imaginecode.thecomebacks.whatsthat.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired UserRepository userRepository;
	@Autowired BlockchainService blockchainService;
	
	public boolean isUserLogged(UserResponse ur) {
		User user = userRepository.findFirstByUsername(ur.getUser());
		return (user != null && user.getHash().equals(ur.getHash())) ? true : false;
	}
	
	public boolean createUser(UserResponse ur) throws Exception {
		User user = new User();
		user.setUsername(ur.getUser());
		user.setHash(ur.getHash());
		user.setAddress(blockchainService.createAdress());
		userRepository.save(user);
		return (user != null && user.getId() != 0) ? true : false;
	}
	
}
