package imaginecode.thecomebacks.whatsthat.service;

import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.AmazonRekognitionException;
import com.amazonaws.services.rekognition.model.DetectLabelsRequest;
import com.amazonaws.services.rekognition.model.DetectLabelsResult;
import com.amazonaws.services.rekognition.model.Label;

import imaginecode.thecomebacks.whatsthat.beans.UserResponse;
import imaginecode.thecomebacks.whatsthat.entity.Image;
import imaginecode.thecomebacks.whatsthat.entity.User;
import imaginecode.thecomebacks.whatsthat.repository.ImageRepository;
import imaginecode.thecomebacks.whatsthat.repository.UserRepository;

@Service
public class ImageService {
	
	@Autowired UserRepository userRepository;
	@Autowired ImageRepository imageRepository;
	@Autowired BlockchainService blockchainService;
	
	public Image getImage(UserResponse ur) {
		User user = userRepository.findFirstByUsername(ur.getUser());
		if(user != null && user.getHash().equals(ur.getHash())) {
			if(user.getImage() != null) {
				return imageRepository.findFirstByOrderByIdAsc(user.getImage().getId()).get(0); 
			}else {
				return imageRepository.findAll().iterator().next(); 
			}
		}
		return null;
	}
	
	public boolean addImage(UserResponse ur) throws Exception {
		User user = userRepository.findFirstByUsername(ur.getUser());
		Image image = null;
		if(user != null && user.getHash().equals(ur.getHash())) {
			image = new Image();
			image.setBase64(ur.getResponse());
			byte[] imageBytes = Base64.getDecoder().decode(ur.getResponse());
			String awsresponse = callRekognition(imageBytes);
			if(awsresponse != null && !awsresponse.isEmpty()) {
				image.setResponse(awsresponse);
				imageRepository.save(image);
			}
		}
		return (image != null && image.getId() != 0) ? true : false;
	}

	private String callRekognition(byte[] image) {
		AWSCredentials credentials = new EnvironmentVariableCredentialsProvider().getCredentials();
        
        AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder
          		.standard()
          		.withRegion(Regions.EU_WEST_1)
        		.withCredentials(new AWSStaticCredentialsProvider(credentials))
        		.build();
        DetectLabelsRequest detectLabels = new DetectLabelsRequest()
        		.withMaxLabels(10)
        		.withMinConfidence(0f)
        		.withImage(
        				new com.amazonaws.services.rekognition.model.Image()
        				.withBytes(ByteBuffer.wrap(image)));
        String resultStr = "";
        try {

        	DetectLabelsResult result = rekognitionClient.detectLabels(detectLabels);
            List<Label> labels = result.getLabels();
            for (Label label: labels) {
            	resultStr+= label.getName()+":"+label.getConfidence()+";";
            }

        } catch (AmazonRekognitionException e) {
            e.printStackTrace();
        }
        return resultStr;
	}
}
