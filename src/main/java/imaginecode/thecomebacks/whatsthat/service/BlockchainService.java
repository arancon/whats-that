package imaginecode.thecomebacks.whatsthat.service;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;

import imaginecode.thecomebacks.whatsthat.beans.UserResponse;
import imaginecode.thecomebacks.whatsthat.contracts.WhatsThat;
import imaginecode.thecomebacks.whatsthat.entity.Image;
import imaginecode.thecomebacks.whatsthat.entity.User;
import imaginecode.thecomebacks.whatsthat.repository.ImageRepository;
import imaginecode.thecomebacks.whatsthat.repository.UserRepository;

@Service
public class BlockchainService{

	@Autowired UserRepository userRepository;
	@Autowired ImageRepository imageRepository;

	@Value("${contract}") private String CONTRACT;
	@Value("${ganache}") private String GANACHE;
	@Value("${privatekey}") private String PRIVATE_KEY;
	@Value("${wallet}") private String WALLET;
	
	private WhatsThat contract;

	public BlockchainService() {
	}
	
	@SuppressWarnings("deprecation")
	@PostConstruct
	public void construct() throws IOException {
		Web3j web3 = Web3j.build(new HttpService(GANACHE));
		Credentials credentials = Credentials.create(PRIVATE_KEY);
		contract = WhatsThat.load( CONTRACT, web3, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
		System.out.println(contract.isValid()?"El contrato es valido!!!":"Oh Oh!");
	}
	
	public BigInteger getPoints(UserResponse ur) throws Exception {
		User user = userRepository.findFirstByUsername(ur.getUser());
		if(user != null && user.getHash().equals(ur.getHash())) {
			return contract.getPoints(user.getAddress()).send();
		}
		return BigInteger.valueOf(0l);
	}
	
	public String createAdress() throws Exception {
		String filenameWallet = WalletUtils.generateNewWalletFile("password", new File(WALLET), false);
		Credentials credentials = WalletUtils.loadCredentials("password", WALLET+filenameWallet);
		
		return credentials.getAddress();
	}
	
	public void calculatePoints(String address, long points) throws Exception {
		contract.addPoints(address, BigInteger.valueOf(points)).send();
	}
	
	public boolean answer(UserResponse ur) throws Exception {
		User user = userRepository.findFirstByUsername(ur.getUser());
		if(user != null && user.getHash().equals(ur.getHash())) {
			Optional<Image> im = imageRepository.findById(ur.getImage());
			if (im.isPresent()) {
				Image image = im.get();
				for(String s : image.getResponse().split(";")) {
					String[] awsword = s.split(":");
					if(awsword[0].equalsIgnoreCase(ur.getResponse())) {
						long points = (long) (Float.parseFloat(awsword[1])*1000);
						contract.addPoints(user.getAddress(), BigInteger.valueOf(points)).send();
						break;
					}
				}
				user.setImage(image);
				userRepository.save(user);
				return true;
			}else {
				return false;
			}

		}
		return false;
	}

}
