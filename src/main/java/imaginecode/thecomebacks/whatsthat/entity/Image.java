package imaginecode.thecomebacks.whatsthat.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the images database table.
 * 
 */
@Entity
@Table(name="images")
@NamedQuery(name="Image.findAll", query="SELECT i FROM Image i")
@JsonIgnoreProperties(value={"users"}, ignoreUnknown = true)
public class Image implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Lob
	private String base64;
	
	@Lob
	private String response;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="image")
	private List<User> users;

	public Image() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBase64() {
		return this.base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setImage(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setImage(null);

		return user;
	}

}